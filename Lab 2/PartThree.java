import java.util.*;
public class PartThree{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		AreaComputations ac = new AreaComputations();
		System.out.println("Enter a side of the square:");
		int squareSide = sc.nextInt();
		System.out.println("Enter a side of the width:");
		int rectangleWidth = sc.nextInt();
		System.out.println("Enter a side of the length:");
		int rectangleLength = sc.nextInt();
		AreaComputations.areaSquare(squareSide);
		ac.areaRectangle(rectangleLength, rectangleWidth);
		System.out.println("Area of square: " + AreaComputations.areaSquare(squareSide));
		System.out.println("Area of rectangle: " + ac.areaRectangle(rectangleLength, rectangleWidth));
	}
}