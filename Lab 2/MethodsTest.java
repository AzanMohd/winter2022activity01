public class MethodsTest{
	public static void main(String[] args){
		SecondClass sc= new SecondClass();
		int x =10;
		int y=10;
		int z=10;
		double doubleZ =3.5; 
		int returnX = methodNoInputNoReturnInt();
		int num1 =3;
		int num2 =6;
		double returnSquare= sumSquareRoot(num1, num2);
		String s1 = "hello";
		String s2 = "goodbye";
		int length1 = s1.length();
		int length2 = s2.length();
		int second1= sc.addOne();
		int second2= sc.addTwo();
		System.out.println(x);
		methodNoInputNoReturn();
		System.out.println(x);
		methodOneInputNoReturn(10);
		methodOneInputNoReturn(x);
		methodOneInputNoReturn(x+50);
		methodTwoInputNoReturn(z, doubleZ);
		System.out.println(returnX);
		System.out.println(returnSquare);
		System.out.println(length1);
		System.out.println(length2);
		System.out.println(second1);
		System.out.println(second2);
	}
	public static void methodNoInputNoReturn(){
		System.out.println("I’m in a method that takes no input and returns nothing");
		int x = 50;
		System.out.println(x);
	}
	public static void methodOneInputNoReturn(int y){
		System.out.println("Inside the method one input no return");
		System.out.println(y);
	}
	public static void methodTwoInputNoReturn(int z, double doubleZ){
		System.out.println("Inside the method two input no return");
		System.out.println(z);
		System.out.println(doubleZ);
	}
	public static int methodNoInputNoReturnInt(){
		return 6;
	}
	public static double sumSquareRoot(int num1, int num2){
		double squareResult = Math.sqrt(6+3);
		return squareResult;
	}
}