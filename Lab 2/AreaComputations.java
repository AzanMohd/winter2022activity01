public class AreaComputations{
	public static int areaSquare(int squareSide){
		return squareSide*squareSide;
	}
	public int areaRectangle(int rectangleWidth, int rectangleLength){
		return rectangleLength*rectangleWidth;
	}
}